//
//  DetailsProviderViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 16/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import MapKit
class DetailsProviderViewController: UIViewController  , MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!

    @IBOutlet weak var nameProvidee: UILabel!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var add: UILabel!
    @IBOutlet weak var spec: UILabel!
    
    @IBOutlet weak var caracUserNAme: UILabel!
    @IBOutlet weak var viewImg: UIView!
    var provider : Provider? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        let logo = UIImage(named: "smallIcon")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        // Do any additional setup after loading the view.
        viewImg.layer.masksToBounds = true
        viewImg.layer.cornerRadius = viewImg.frame.width/2
        let v1 : String   = provider!.first_name[0]
        let v2 : String   = provider!.last_name[0]
        
        caracUserNAme.text =  "\(v1.uppercased())\(v2.uppercased())"
        
        
        
        spec.text = provider?.services[0]
        age.text = provider?.age
        add.text = provider?.adress
        nameProvidee.text = (provider?.first_name)! + " " + (provider?.last_name)!
         
        mapView.delegate = self
        
        
        
        let address = provider?.adress
        
        let geoCoder = CLGeocoder()
        geoCoder.geocodeAddressString(address!) { (placemarks, error) in
            guard
                let placemarks = placemarks,
                let location = placemarks.first?.location
                else {
                    // handle no location found
                    print("handle no location found")
                    print(error)
                    return
            }
            
            // Use your location
             let sourcePlacemark = MKPlacemark(coordinate: location.coordinate, addressDictionary: nil)
            let sourceMapItem = MKMapItem(placemark: sourcePlacemark)
            let sourceAnnotation = MKPointAnnotation()
            
            sourceAnnotation.title = "Times Square"
            sourceAnnotation.coordinate = location.coordinate
            
            
            self.mapView.showAnnotations([sourceAnnotation], animated: true )

        }
      
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func callAction(_ sender: Any) {
        let ch:String = "telprompt://\(provider?.tel ?? "0000")"
        if let url = URL(string: ch), UIApplication.shared.canOpenURL(url as URL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

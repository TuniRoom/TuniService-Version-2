//
//  Users.swift
//  tuniservice
//
//  Created by MacBook Pro on 11/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit

class User: NSObject {
    
    var id : String  = ""
    var  email : String = ""
    var first_name : String = ""
    var last_name : String = ""
    var full_name : String = ""
    var token : String = ""
    var password : String = ""
    var salt : String = ""
    var role : String = ""
    var valid : Bool = true
    var type : String = ""
    var profilePictureUrl : String = ""
    var accountType : typeAccount = .Normal
    
    
    

}

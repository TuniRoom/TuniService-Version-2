//
//  menuViewController.swift
//  memuDemo
//
//  Created by Parth Changela on 09/10/16.
//  Copyright © 2016 Parth Changela. All rights reserved.
//

import UIKit
import SideMenu
import FacebookCore
import FacebookLogin
import GoogleSignIn
import Google
class menuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tblTableView: UITableView!
    
    
    var lastSelectedIndex = IndexPath(row: 0, section: 0)
    var firstSelected : Bool = false


    var ManuNameArray:Array = [String]()
     override func viewDidLoad() {
        super.viewDidLoad()
        ManuNameArray = ["Acceuil" , "Profile", "Termes et politique"]
   
        // Do any additional setup after loading the view.
        firstSelected = true

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ManuNameArray.count
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCell", for: indexPath) as! MenuCell
        
        cell.selectionStyle = .none

        
         cell.lblMenuname.text! = ManuNameArray[indexPath.row]
         return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let cell:MenuCell = tableView.cellForRow(at: indexPath) as! MenuCell
         cell.backgroundMenuCell.backgroundColor = UIColor(hexString: "#BF2835")
        cell.lineView.backgroundColor = UIColor(hexString: "#BF2835")
        print(cell.lblMenuname.text!)
        if (cell.lblMenuname.text! == "Termes et politique"){
            //Notification
            let Notification  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Notification")
            self.show(Notification, sender: nil)
        }
        
        if (cell.lblMenuname.text! == "Profile"){
            //Notification
            let Notification  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileViewController") 
             self.show(Notification, sender: nil)
        }
        
        
        if (cell.lblMenuname.text! == "Acceuil"){
            //acceuil
            let acceuil  = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "acceuil")
             self.show(acceuil, sender: nil)
        }
        
      
        
        
  // dismiss(animated: true, completion: nil)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
         let cell:MenuCell = tableView.cellForRow(at: indexPath) as! MenuCell

        // cell.backgroundColor = MyColors.graySideMenu
        // cell.title.textColor = MyColors.dark]
         cell.backgroundMenuCell.backgroundColor = UIColor(hexString: "#D03547")
        cell.lineView.backgroundColor = UIColor(hexString: "#BF2835")
    }
    
    @IBAction func deconnexionAction(_ sender: Any) {
        
        let user : User = Utils.readUserData(from: "Menu")
        if (user.accountType == .Facebook){
            
            let loginManager = LoginManager()
            loginManager.logOut()
            
        }else if (user.accountType == .Google){
            GIDSignIn.sharedInstance().signOut()

        }else {
            
        }
        
        Utils.removeUser()
        
        let window :UIWindow = UIApplication.shared.keyWindow!

        
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var viewcontrollers = self.navigationController?.viewControllers
        viewcontrollers?.removeAll()
        
        let vc = storyboard.instantiateViewController(withIdentifier: "LoginViewController")
        
       self.show(vc, sender: nil)
        
    }

}

//
//  AcceuilViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 10/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage
import NVActivityIndicatorView

class AcceuilViewController: UIViewController , NVActivityIndicatorViewable  {
    
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var V1: UIView!
    @IBOutlet weak var V2: UIView!
    @IBOutlet weak var V3: UIView!
    @IBOutlet weak var V4: UIView!
    @IBOutlet weak var V5: UIView!
    @IBOutlet weak var V6: UIView!
    @IBOutlet weak var homeImg: UIImageView!
    @IBOutlet weak var carImg: UIImageView!
    @IBOutlet weak var healthImg: UIImageView!
    @IBOutlet weak var settingImg: UIImageView!
    @IBOutlet weak var buildingImg: UIImageView!
    
     
    @IBOutlet weak var btnIteamBarNotif: UIBarButtonItem!
    @IBOutlet weak var btnItemBar: UIBarButtonItem!
 
    var obj1 : Category = Category()
    var obj2 : Category = Category()
    var obj3 : Category = Category()
    var obj4 : Category = Category()
    var obj5 : Category = Category()
    var obj6 : Category = Category()
    @IBAction func sideMenuAction(_ sender: Any) {
        present(SideMenuManager.menuRightNavigationController!, animated: true, completion: nil)
    }
    
    @IBOutlet weak var titleBar: UINavigationItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupSideMenu()
        
        titleBar.titleView?.backgroundColor = UIColor.red
        
      //  let logo = UIImage(named: "iconBar")
       // let imageView = UIImageView(image:logo)
        
        
     /*   btnIteamBarNotif.setBadge(text: "3")
        let myimage = UIImage(named: "notif_on")?.withRenderingMode(.alwaysOriginal)
        btnIteamBarNotif.image = myimage*/
      //  self.navigationItem.titleView = imageView
        
        
       
        
        
        
       
        // Set Border To txtSearch
        let myColor : UIColor = UIColor(hexString: "#E3E4E5")
        txtSearch.layer.masksToBounds = true
        txtSearch.layer.borderColor = myColor.cgColor
        txtSearch.layer.borderWidth = 2.0
        
        
        //   set Corner Radius to V1 -> V2  make  around view
        V1.layer.cornerRadius = 6
        V2.layer.cornerRadius = 6
        V3.layer.cornerRadius = 6
        V4.layer.cornerRadius = 6
        V5.layer.cornerRadius = 6
        V6.layer.cornerRadius = 6
        
        
        
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
       txtSearch.text = ""
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
 
        if (NetWork.isConnectedToNetwork() == true ){
            let size = CGSize(width: 60, height: 60)
            self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
            setDataByIDService( id : "598d8ac8a6dbdea19e794d91" , n : 1 )
            setDataByIDService( id : "598d8b12a6dbdea19e794d92" , n : 2 )
            setDataByIDService( id : "598d8b28a6dbdea19e794d93" , n : 3 )
            setDataByIDService( id : "598d8b3aa6dbdea19e794d94" , n : 4 )
            setDataByIDService( id : "598d8b4aa6dbdea19e794d95" , n : 5 )
            setDataByIDService( id : "598d8b59a6dbdea19e794d96" , n : 6 )
            
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            popOverVC.acceuilViewController = self
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
     }
    func tappedView1(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
 
        if (NetWork.isConnectedToNetwork() == true ){
            //Download Photo
            let url = URL(string: self.obj1.mobileIconHover)
            //  photoProfile.kf.setImage(with: url)
            
            
            self.homeImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-home"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                if error != nil {
                    print("Failed: \(error)")
                } else {
                    print("Success")
                }}
            
            
            
            
            
            
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpAutreViewController") as! PopUpAutreViewController
            popOverVC.idCategory = self.obj1._id
            
            popOverVC.titleTxt = ".: \(self.obj1.title) Services :."
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            
            
            popOverVC.acceuilViewController = self
            
            popOverVC.didMove(toParentViewController: self)
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
        
       
        
        
        

    }
    func tappedView2(tapGestureRecognizer: UITapGestureRecognizer)
    {
         if (NetWork.isConnectedToNetwork() == true ){
            
            //Download Photo
            let url = URL(string: self.obj2.mobileIconHover)
            //  photoProfile.kf.setImage(with: url)
            
            
            self.carImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-car"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                if error != nil {
                    print("Failed: \(error)")
                } else {
                    print("Success")
                }}
            
            
            
            
            
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpAutreViewController") as! PopUpAutreViewController
            popOverVC.idCategory = self.obj2._id
            
            popOverVC.titleTxt = ".: \(self.obj2.title) Services :."
            popOverVC.acceuilViewController = self
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            
            popOverVC.didMove(toParentViewController: self)
            
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
        
        
 
    }
    func tappedView3(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
         if (NetWork.isConnectedToNetwork() == true ){
            
            //Download Photo
            let url = URL(string: self.obj3.mobileIconHover)
            //  photoProfile.kf.setImage(with: url)
            
            
            self.healthImg.sd_setImage(with: url, placeholderImage: UIImage(named: "gray-heartbeat"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                if error != nil {
                    print("Failed: \(error)")
                } else {
                    print("Success")
                }}
            
            
            
            
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpAutreViewController") as! PopUpAutreViewController
            popOverVC.idCategory = self.obj3._id
            
            popOverVC.titleTxt = ".: \(self.obj3.title) Services :."
            popOverVC.acceuilViewController = self
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            
            popOverVC.didMove(toParentViewController: self)
            
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
        
        
        
    }
    
    func tappedView4(tapGestureRecognizer: UITapGestureRecognizer)
    {
      if (NetWork.isConnectedToNetwork() == true ){
            
        //Download Photo
        let url = URL(string: self.obj4.mobileIconHover)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.settingImg.sd_setImage(with: url, placeholderImage: UIImage(named: "gray-settings"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}
        
        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpAutreViewController") as! PopUpAutreViewController
        popOverVC.acceuilViewController = self
        popOverVC.idCategory = self.obj4._id
        
        popOverVC.titleTxt = ".: \(self.obj4.title) Services :."
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        
        popOverVC.didMove(toParentViewController: self)
            
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
        
        
    }
    func tappedView5(tapGestureRecognizer: UITapGestureRecognizer)
    {
        
         if (NetWork.isConnectedToNetwork() == true ){
            
            //Download Photo
            let url = URL(string: self.obj5.mobileIconHover)
            //  photoProfile.kf.setImage(with: url)
            
            
            self.buildingImg.sd_setImage(with: url, placeholderImage: UIImage(named: "gray-building"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                if error != nil {
                    print("Failed: \(error)")
                } else {
                    print("Success")
                }}
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpAutreViewController") as! PopUpAutreViewController
            popOverVC.acceuilViewController = self
            popOverVC.idCategory = self.obj5._id
            
            popOverVC.titleTxt = ".: \(self.obj5.title) Services :."
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            
            popOverVC.didMove(toParentViewController: self)
            
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
    }
    
   
    @IBAction func otherAction(_ sender: Any) {
        
    
        if (NetWork.isConnectedToNetwork() == true){
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "PopUpAutreViewController") as! PopUpAutreViewController
            
            popOverVC.titleTxt = ".: Autre Services :."
            popOverVC.acceuilViewController = self
            popOverVC.idCategory = self.obj6._id
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            
            popOverVC.didMove(toParentViewController: self)
            

        }else {
                /*************** Check your Network **************/
                
                let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
                self.addChildViewController(popOverVC)
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                popOverVC.didMove(toParentViewController: self)
                /************** End Check your network *************/
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

 
    fileprivate func setupSideMenu() {
        let v  : menuViewController =  storyboard!.instantiateViewController(withIdentifier: "menuViewController") as! menuViewController
 
        let menuLeftNavigationController = UISideMenuNavigationController(rootViewController: v)
        menuLeftNavigationController.leftSide = true
        
        menuLeftNavigationController.navigationBar.isHidden = true
        
        SideMenuManager.menuPushStyle = .defaultBehavior
        SideMenuManager.menuRightNavigationController = menuLeftNavigationController
        SideMenuManager.menuLeftNavigationController = nil
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
        
        SideMenuManager.menuPresentMode = .viewSlideInOut
        SideMenuManager.menuFadeStatusBar = false
        
        SideMenuManager.menuAnimationBackgroundColor = UIColor.gray
        SideMenuManager.menuShadowRadius = 0.0
        
        SideMenuManager.menuDismissOnPush = true
        SideMenuManager.menuAllowPushOfSameClassTwice = false
    }
    
    
    
    
    func setDataByIDService( id : String , n : Int){
        
        //Category
        

        
        
        AFWrapper.requestGETURL("https://tuniservices-server.herokuapp.com/api/v1/categories/\(id)?fields=%7B%22mobileIcon%22:1,%22mobileIconHover%22:1,%22fr.title%22:1%7D", success: {
            (JSONResponse) -> Void in
            
            
            
            if (JSONResponse["status"].int == 400 ) {
                
                    self.stopAnimating()
                    Utils.snackBar(message: "Vérifier votre connexion internet")
                    self.dismissKeyboard()
                
                
            }else {
                
                
                
                let streamsJsonData = JSONResponse["data"]
                print(streamsJsonData)
                let category : Category = Category()
                category._id = streamsJsonData["_id"].string!
                category.mobileIcon = streamsJsonData["mobileIcon"].string!
                category.mobileIconHover = streamsJsonData["mobileIconHover"].string!
                category.title = streamsJsonData["fr"]["title"].string!
                
                if ( n == 1 ) {
                    self.obj1 = category
                    
                    //Download Photo
                    let url = URL(string: self.obj1.mobileIcon)
                    //  photoProfile.kf.setImage(with: url)
                    
                    
                    self.homeImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-home"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                        if error != nil {
                            print("Failed: \(error)")
                        } else {
                            print("Success")
                            // Add Action To VIEWS
                            
                            let tapGestureRecognizer1 = UITapGestureRecognizer(target: self, action: #selector(self.tappedView1(tapGestureRecognizer:)))
                            self.V1.isUserInteractionEnabled = true
                            self.V1.addGestureRecognizer(tapGestureRecognizer1)
                            
                          
                            

                        }}
                    
                    
                }
                if ( n == 2 ) {
                    self.obj2 = category
                    
                    
                    //Download Photo
                    let url = URL(string: self.obj2.mobileIcon)
                    //  photoProfile.kf.setImage(with: url)
                    
                    
                    self.carImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-car"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                        if error != nil {
                            print("Failed: \(error)")
                        } else {
                            print("Success")
                            
                            let tapGestureRecognizer2 = UITapGestureRecognizer(target: self, action: #selector(self.tappedView2(tapGestureRecognizer:)))
                            self.V2.isUserInteractionEnabled = true
                            self.V2.addGestureRecognizer(tapGestureRecognizer2)
                            
                           
                        }}
                }
                if ( n == 3 ) {
                    self.obj3 = category
                    
                    
                    
                    //Download Photo
                    let url = URL(string: self.obj3.mobileIcon)
                    //  photoProfile.kf.setImage(with: url)
                    
                    
                    self.healthImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-heartbeat"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                        if error != nil {
                            print("Failed: \(error)")
                        } else {
                            print("Success")
                            
                            let tapGestureRecognizer3 = UITapGestureRecognizer(target: self, action: #selector(self.tappedView3(tapGestureRecognizer:)))
                            self.V3.isUserInteractionEnabled = true
                            self.V3.addGestureRecognizer(tapGestureRecognizer3)
                            
                           
                        }}
                    
                }
                if ( n == 4 ) {
                    self.obj4 = category
                    
                    //Download Photo
                    let url = URL(string: self.obj4.mobileIcon)
                    //  photoProfile.kf.setImage(with: url)
                    
                    
                    self.settingImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-settings"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                        if error != nil {
                            print("Failed: \(error)")
                        } else {
                            print("Success")
                            let tapGestureRecognizer4 = UITapGestureRecognizer(target: self, action: #selector(self.tappedView4(tapGestureRecognizer:)))
                            self.V4.isUserInteractionEnabled = true
                            self.V4.addGestureRecognizer(tapGestureRecognizer4)
                          
                        }}
                }
                if (n == 6 ){
                    
                    self.obj6 = category
                    self.stopAnimating()
                }
                if ( n == 5 ) {
                    self.obj5 = category
                    
                    
                    //Download Photo
                    let url = URL(string: self.obj5.mobileIcon)
                    //  photoProfile.kf.setImage(with: url)
                    
                    
                    self.buildingImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-building"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
                        if error != nil {
                            print("Failed: \(error)")
                        } else {
                            print("Success")
                            
                            let tapGestureRecognizer5 = UITapGestureRecognizer(target: self, action: #selector(self.tappedView5(tapGestureRecognizer:)))
                            self.V5.isUserInteractionEnabled = true
                            self.V5.addGestureRecognizer(tapGestureRecognizer5)
                        }}
                }
                
                
      

                
                
            }
            
            
            
            
           
          }) {
            (error) -> Void in
            print(error)
        }
        
    }
    
    func rest(){
        //Download Photo
        let url = URL(string: self.obj1.mobileIcon)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.homeImg.sd_setImage(with: url, placeholderImage: UIImage(named:"gray-home"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}
        
        
        
        
        
        //Download Photo
        let url2 = URL(string: self.obj2.mobileIcon)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.carImg.sd_setImage(with: url2, placeholderImage: UIImage(named:"gray-car"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}
        
        
        
        
        //Download Photo
        let url3 = URL(string: self.obj3.mobileIcon)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.healthImg.sd_setImage(with: url3, placeholderImage: UIImage(named:"gray-heartbeat"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}
        
        
        
        
        //Download Photo
        let url4 = URL(string: self.obj4.mobileIcon)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.settingImg.sd_setImage(with: url4, placeholderImage: UIImage(named:"gray-settings"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}

        
        
        //Download Photo
        let url5 = URL(string: self.obj5.mobileIcon)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.buildingImg.sd_setImage(with: url5, placeholderImage: UIImage(named:"gray-building"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}
        
    }
    
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func searchAction(_ sender: UITextField) {
        //txtSearch
     
        
        if (NetWork.isConnectedToNetwork() == true ){
           
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SearchServiceViewController") as! SearchServiceViewController
            
            self.show(popOverVC, sender: nil)

            
            
        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
        
        
    }
    
  


}

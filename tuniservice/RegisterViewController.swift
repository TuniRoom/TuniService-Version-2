//
//  RegisterViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 14/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class RegisterViewController: UIViewController , NVActivityIndicatorViewable  {

    @IBOutlet weak var btnValider: UIButton!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

         let myColor : UIColor = UIColor(hexString: "#E3E4E5")
        txtLastName.layer.masksToBounds = true
        txtLastName.layer.borderColor = myColor.cgColor
        txtLastName.layer.borderWidth = 2.0
        txtLastName.layer.cornerRadius = 4
        txtFirstName.layer.masksToBounds = true
        txtFirstName.layer.borderColor = myColor.cgColor
        txtFirstName.layer.borderWidth = 2.0
        txtFirstName.layer.cornerRadius = 4
        txtPassword.layer.masksToBounds = true
        txtPassword.layer.borderColor = myColor.cgColor
        txtPassword.layer.borderWidth = 2.0
        txtPassword.layer.cornerRadius = 4
        txtEmail.layer.masksToBounds = true
        txtEmail.layer.borderColor = myColor.cgColor
        txtEmail.layer.borderWidth = 2.0
        txtEmail.layer.cornerRadius = 4
        btnValider.layer.cornerRadius = 5
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func validerAction(_ sender: Any) {
         if (NetWork.isConnectedToNetwork() == true){
            /********************** Check Data *********************/
            
                let emailTxt = txtEmail.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                let firstNameTxt = txtFirstName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                let lastNameTxt = txtLastName.text?.trimmingCharacters(in: .whitespacesAndNewlines)
                let passwordTxt = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            var checked : Bool = false
            if ( (emailTxt == "") && (firstNameTxt == "") && (lastNameTxt == "") && (passwordTxt == "") ){
                self.txtEmail.layer.borderColor = UIColor.red.cgColor
                self.txtFirstName.layer.borderColor = UIColor.red.cgColor
                self.txtLastName.layer.borderColor = UIColor.red.cgColor
                self.txtPassword.layer.borderColor = UIColor.red.cgColor
                checked = false
                Utils.snackBar(message: "Remplir tous les données !!")
                self.dismissKeyboard()
            }
            
            if (emailTxt == "") {
                self.txtEmail.layer.borderColor = UIColor.red.cgColor
                checked = false

                if ((firstNameTxt == "") && (lastNameTxt == "") && (passwordTxt == "")){
                    
                } else {
                    if (Utils.isValidEmail(testStr: emailTxt!)) {
                        checked = true

                        self.txtFirstName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
                    }else {
                        self.txtFirstName.layer.borderColor = UIColor.red.cgColor
                        checked = false
                        Utils.snackBar(message: "Vérifier votre Email")
                        self.dismissKeyboard()
                    }
               
                }
                
            }else {
                self.txtEmail.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
                checked = true

            }
            
            
            
            if (firstNameTxt == "") {
                
                self.txtFirstName.layer.borderColor = UIColor.red.cgColor
                checked = false

                if ((emailTxt == "") && (lastNameTxt == "") && (passwordTxt == "")){
                    
                }else {
                    Utils.snackBar(message: "Vérifier votre First Name")
                    self.dismissKeyboard()

                    checked = false

                }
                
            }else {
                
                
                self.txtFirstName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
                
                checked = true

            }
            
            
            
            if (lastNameTxt == "") {
                self.txtLastName.layer.borderColor = UIColor.red.cgColor
                checked = false

                if ((firstNameTxt == "") && (emailTxt == "") && (passwordTxt == "")){
                    
                }else {
                    Utils.snackBar(message: "Vérifier votre Last Name")
                    checked = false

                    self.dismissKeyboard()
                }
                
            }else {
                self.txtLastName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
                checked = true

            }
            if (passwordTxt == "") {
                self.txtPassword.layer.borderColor = UIColor.red.cgColor
                checked = false

                if ((firstNameTxt == "") && (emailTxt == "") && (lastNameTxt == "")){
                    
                }else {
                    checked = false

                    Utils.snackBar(message: "Vérifier votre Last Name")
                    self.dismissKeyboard()
                }
                
            }else {
                checked = true

                self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
            }
            
            
            /*******************************************************/
            
            if (checked == true ){
                /*************************** Register From Server ***********************/
                
                let url = "https://tuniservices-server.herokuapp.com/api/v1/auth/register"
                let param : [String : String ] = ["email": self.txtEmail.text!,
                                                  "password": self.txtPassword.text! ,
                                                  "first_name" : self.txtFirstName.text!,
                                                  "last_name" : self.txtLastName.text!,
                                                  "profilePictureUrl" : "https://www.sapientglobalmarkets.com/wp-content/uploads/2015/07/profilepic.jpg"]
                
                
                let size = CGSize(width: 60, height: 60)
                self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
                
                AFWrapper.requestPOSTURL(url, params: param as [String : AnyObject], success: {   (JSONResponse) -> Void in
                    print(JSONResponse)
                    
                    if (JSONResponse["status"].int == 400 ) {
                        
                        let msgErr = JSONResponse["errors"].string
                        if ( msgErr == "The Information you provided is incomplete") {
                            //  Utils.snackBar(message: "Votre compte n'existe pas!!")
                            Utils.snackBar(message: "Votre email déja existe")

                            self.dismissKeyboard()
                            
                            
                            
                        } else {
                            // Utils.snackBar(message: "Mot de passe incorrecte")
                            Utils.snackBar(message: "Votre email déja existe")
                            
                            self.dismissKeyboard()
                         }
                        
                    }else {
                        
                        let data = JSONResponse["data"]
                        let user : User = User()
                        user.token = data["token"].string!
                        let objUser = data["user"]
                        user.email = objUser["email"].string!
                        user.id = objUser["_id"].string!

                        user.first_name = objUser["first_name"].string!
                        user.last_name = objUser["last_name"].string!

                        user.profilePictureUrl =  objUser["profilePictureUrl"].string!
                        
                        
                        Utils.saveUserData(user: user, type: .Normal)
                        self.stopAnimating()
                        
                        
                        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "startMainViewController") as! UINavigationController

                        self.show(popOverVC, sender: nil)
                    }
                    self.stopAnimating()
                    
                    
                }, failure: {  (error) -> Void in
                    print(error)
                    
                    
                    self.stopAnimating()
                    
                    // 400 (  "Wrong Password"  )
                    // This String Should Be an Email
                    Utils.snackBar(message: "Vérifier votre connection internet  !!")
                    self.dismissKeyboard()
                    
                    
                    
                })
                
                
                
                /*************************** END Register From Server ***********************/
            }
            
            
            
           

        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        }
        
     }
    

    @IBAction func seConnecterAction(_ sender: Any) {
        //self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    @IBAction func EmailColorBorderAction(_ sender: UITextField) {
        self.txtEmail.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtFirstName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtLastName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor




    }
    @IBAction func PasswordColorBorderAction(_ sender: Any) {
        self.txtEmail.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtFirstName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtLastName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
    }
    
    
    @IBAction func FirstNameColorBorderAction(_ sender: UITextField) {
        self.txtEmail.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtFirstName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtLastName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
    }
    @IBAction func LastNameColorBorderAction(_ sender: Any) {
        self.txtEmail.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtFirstName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtLastName.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
    }

}

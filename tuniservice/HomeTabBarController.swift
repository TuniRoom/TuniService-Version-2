//
//  HomeTabBarController.swift
//  M-Dinar
//
//  Created by MacBook Pro on 11/07/2017.
//  Copyright © 2017 Levelip. All rights reserved.
//

import UIKit
import SideMenu
  class HomeTabBarController: UITabBarController {

    @IBOutlet weak var btnIteamBarNotif: UIBarButtonItem!
    @IBOutlet weak var btnItemBar: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.tabBar.backgroundColor = UIColor(hexString: "#f1b95f")
      //  self.tabBar.barTintColor = UIColor(hexString: "#e67e22")
        self.tabBar.unselectedItemTintColor = UIColor(hexString: "#4f4f4f")
        self.tabBar.tintColor = UIColor(hexString: "CE1B36")
        //print("********************************  \(self.tabBarController?.viewControllers?.count) ")
        // Do any additional setup after loading the view.
        
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the view controller it displays!
      
        
        
    }
    
    @IBAction func sideMenuAction(_ sender: Any) {
        present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    // UITabBarDelegate
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
    }
    
    // UITabBarControllerDelegate
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        print("Selected view controller")
    }
    
    
    

}

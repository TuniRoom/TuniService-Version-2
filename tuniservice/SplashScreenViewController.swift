//
//  SplashScreenViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 17/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import Lottie

class SplashScreenViewController: UIViewController {
    var lottieLogo: LOTAnimationView!
    @IBOutlet weak var vv: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
              // Do any additional setup after loading the view.

        // Do any additional setup after loading the view.
        perform(Selector("showNavController"), with: nil, afterDelay: 3)
        
        
        
        
        
        let animationView = LOTAnimationView.init(name: "tuni_anim")
        animationView.backgroundColor = UIColor.clear
        animationView.tintColor = UIColor.black
        animationView.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        animationView.contentMode = .scaleAspectFill
        animationView.loopAnimation = true
        
        self.vv.addSubview(animationView)
        
        animationView.play()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func showNavController(){
        if (Utils.userDataExist() == true){
            performSegue(withIdentifier: "segueShowMainApps", sender: self)
        }else {
            performSegue(withIdentifier: "showSplashScreen", sender: self)
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

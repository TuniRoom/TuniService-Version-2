//
//  PopUpAutreViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 10/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class PopUpAutreViewController: UIViewController , UITableViewDelegate , UITableViewDataSource , NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
   
    
    @IBOutlet weak var titre: UILabel!
   
    var titleTxt = ""
    
    var acceuilViewController : AcceuilViewController? = nil
    
    var idCategory = ""
    
    
    var services :  [Service] = []

   
    @IBOutlet weak var viewClose: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.showAnimate()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        titre.text = titleTxt
       
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tappedClose(tapGestureRecognizer:)))
            viewClose.isUserInteractionEnabled = true
            viewClose.addGestureRecognizer(tapGestureRecognizer)
        
        
        
        
        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor.clear
        GetDATA()
        
    }
    func tappedClose(tapGestureRecognizer: UITapGestureRecognizer)
    {
        self.removeAnimate()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*************************** Table View *************************/
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCellHomeService", for: indexPath) as! myCellHomeService
        
        cell.imgService.isHidden  = true

        
        cell.nameService.text = services[indexPath.row].service
        
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "myCellHomeService", for: indexPath) as! myCellHomeService
        print("yuppp Clicked ")
        
        
        
        cell.imgService.isHidden  = false

        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListProviderViewController") as! ListProviderViewController
        popOverVC.service = services[indexPath.row]
        self.show(popOverVC, sender: nil)
        
        
        self.removeAnimate()

        
    }
    
    /************************ End Table View *************************/

    /********** Animation ************/
    
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
        
        
        acceuilViewController?.rest()
    }
    
    /********** End Animation ************/
    
    func GetDATA(){
        if (NetWork.isConnectedToNetwork() == true){
            self.services = []
            
            let size = CGSize(width: 60, height: 60)
            self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
            
  
            
            
            AFWrapper.requestGETURL("https://tuniservices-server.herokuapp.com/api/v1/categories/\(idCategory)?fields=%7B%22fr.services%22:1%7D", success: {
                (JSONResponse) -> Void in
                
                let streamsJsonData = JSONResponse["data"]
                let streamsService = streamsJsonData["fr"]["services"].array
                
                print(streamsJsonData)
                if (streamsService != nil ) {
                    for i in streamsService! {
                        let s : Service = Service()
                        s.service = i["service"].string!
                        s.descrip = i["description"].string!
                        self.services.append(s)
                    }
                    self.stopAnimating()
                }
              
                self.tableView.reloadData()
             }) {
                (error) -> Void in
                print(error)
                 self.stopAnimating()
            }

        }else {
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
             self.removeAnimate()

        }
     
        
    }


}

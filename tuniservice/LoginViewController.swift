//
//  LoginViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 11/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import Google
import GoogleSignIn
import Lottie
import TTGSnackbar
import NVActivityIndicatorView
class LoginViewController: UIViewController , LoginButtonDelegate ,  GIDSignInUIDelegate, GIDSignInDelegate , NVActivityIndicatorViewable  {

    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtLogin: UITextField!
    @IBOutlet weak var btnValider: UIButton!
    
    @IBOutlet weak var btnGmail: UIButton!
    
    @IBOutlet weak var btnFacebook: UIButton!
    
 
    
    let googleSignInButton = GIDSignInButton()

    var ME : LoginViewController? = nil
    var  startMainViewController   : UINavigationController? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ME = self
       startMainViewController =  storyboard!.instantiateViewController(withIdentifier: "startMainViewController") as! UINavigationController

        // Set Border To txtSearch
        let myColor : UIColor = UIColor(hexString: "#E3E4E5")
        txtPassword.layer.masksToBounds = true
        txtPassword.layer.borderColor = myColor.cgColor
        txtPassword.layer.borderWidth = 2.0
        txtPassword.layer.cornerRadius = 4
        txtLogin.layer.masksToBounds = true
        txtLogin.layer.borderColor = myColor.cgColor
        txtLogin.layer.borderWidth = 2.0
        txtLogin.layer.cornerRadius = 4
        btnValider.layer.cornerRadius = 5
     
        
        
        btnFacebook.layer.cornerRadius = 5
        btnGmail.layer.cornerRadius = 5
        
        
        let loginButton = LoginButton(readPermissions: [ .publicProfile, .email, .userFriends ])
        loginButton.center = view.center
        loginButton.delegate = self
        
        
       
        
        
        
        //error object
        var error : NSError?
        
        //setting the error
        GGLContext.sharedInstance().configureWithError(&error)
        
        //if any error stop execution and print error
        if error != nil{
            print(error ?? "google error")
            return
        }
        
   
        //adding the delegates
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        /*
        //getting the signin button and adding it to view
        googleSignInButton.center = view.center
        view.addSubview(googleSignInButton)
        */
        
    
    }
    
    
    @IBAction func googleSignInAction(_ sender: Any) {
        
        
        if (NetWork.isConnectedToNetwork() == true){
            
            let size = CGSize(width: 60, height: 60)
            self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
            
          GIDSignIn.sharedInstance().signIn()
          
        
        } else {
            
            print("****************** No Internet  Connection")
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
            
         
            
        }
        
        
        
    }
    
    
    //when the signin complets
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if ( NetWork.isConnectedToNetwork() == true ){
            
            /******************** Connection Google ******************/
            
            //if any error stop and print the error
            if error != nil{
                print("errrrrrrrrrrrrrrrrr GOOGLE Erreur")

                print(error ?? "google error")
                return
            }
            //if success display the email on label
            print(user.profile.email)
            print("user.profile.familyName" , user.profile.familyName)
            print("givenName" , user.profile.givenName)
            print("name" , user.profile.name)
            print(user.accessibleScopes)
            print("url image ", signIn.currentUser.profile.imageURL(withDimension: 400))
            let currentUserGG : User = User()
            currentUserGG.email = user.profile.email
            currentUserGG.first_name = user.profile.familyName
            currentUserGG.last_name = user.profile.givenName
            currentUserGG.full_name = user.profile.name
            currentUserGG.id = user.userID
            currentUserGG.profilePictureUrl = "\(signIn.currentUser.profile.imageURL(withDimension: 400))"
            print("TOKEEEENNN FROM SERVER GMAIL : ",user.authentication.accessToken)
            
         
            
            let url = "https://tuniservices-server.herokuapp.com/api/v1/auth/login/google"
            let param = ["email": currentUserGG.email,
                         "token": user.authentication.accessToken ,
                         "first_name" : "\(currentUserGG.first_name)",
                "last_name" : "\(currentUserGG.last_name)",
                "profilePictureUrl" : currentUserGG.profilePictureUrl]
            AFWrapper.requestPOSTURL(url, params: param as [String : AnyObject], success: {   (JSONResponse) -> Void in
                print(JSONResponse)
                
                
                
                if (JSONResponse["status"].int == 400 ) {
                    
                    let msgErr = JSONResponse["errors"].string
                    if ( msgErr == "User Not Found") {
                        //  Utils.snackBar(message: "Votre compte n'existe pas!!")
                        self.dismissKeyboard()
                    
                        
                    } else {
                        // Utils.snackBar(message: "Mot de passe incorrecte")
                        self.dismissKeyboard()
                        
                    }
                    
                }else {
                    
                    let data = JSONResponse["data"]
                    let user : User = User()
                    if ( data["token"] == nil ){
                        user.token = ""
                    }else {
                        user.token = data["token"].string!

                    }
                    let objUser = data["user"]
                    user.email = objUser["email"].string!
                    user.id = objUser["_id"].string!
                    user.first_name = objUser["first_name"].string!
                    user.last_name = objUser["last_name"].string!
                     user.profilePictureUrl =  objUser["profilePictureUrl"].string!
                    
                    Utils.saveUserData(user: user, type: .Google)
                    
                    self.show(self.startMainViewController!, sender: nil)
                    
                    self.stopAnimating()
                    
                    
                }
                
                self.stopAnimating()
            
            }, failure: {  (error) -> Void in
                print(error)
                
                print("errrrrrrrrrrrrrrrrr GOOGLE")
                // 400 (  "Wrong Password"  )
                // This String Should Be an Email
                Utils.snackBar(message: "Vérifier votre connection internet  !!")
                self.dismissKeyboard()
                
                
            })

            
            /**********************************************************/
           }else {
            print("****************** No Internet  Connection")
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
            
        }
        
     }

    
    @IBAction func validerAction(_ sender: Any) {
        
       

      
        
        if (NetWork.isConnectedToNetwork() == true){
            
            
            /************************** Check Input Data *********************/
            var checked : Bool = true
            let loginTxt = txtLogin.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            let passwordTxt = txtPassword.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if ( loginTxt == "") {
                  self.txtLogin.layer.borderColor = UIColor.red.cgColor
                  Utils.snackBar(message: "Vérifier votre Email !!")
                self.dismissKeyboard()

                  checked = false
                
             } else {
                
                if ( Utils.isValidEmail(testStr: loginTxt!) ) {
                    
                    self.txtLogin.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
                    
                        checked = true
                    
                } else {
                    Utils.snackBar(message: "Vérifier votre Email !!")
                    self.dismissKeyboard()


                     self.txtLogin.layer.borderColor = UIColor.red.cgColor
                    checked = false

                }
  
            }
            
            
            if ( passwordTxt == ""){
                self.txtPassword.layer.borderColor = UIColor.red.cgColor
                checked = false
                if ( loginTxt == ""){
                    
                }else {
                    Utils.snackBar(message: "Vérifier votre Password !!")
                    self.dismissKeyboard()

                }

            } else {
                checked = true
                self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
            
            }
            print(checked)
            /************************** End check input Data *****************/
            
            
            
            /************************** MY SERVICE WEB *******************/
            if (checked == true){
                let size = CGSize(width: 60, height: 60)
                self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
                
                 //https://tuniservices-server.herokuapp.com/api/v1/auth/login
                 let url = "https://tuniservices-server.herokuapp.com/api/v1/auth/login"
                 let param = [ "email" : txtLogin.text , "password" : txtPassword.text]
                 AFWrapper.requestPOSTURL(url, params: param as [String : AnyObject], success: {   (JSONResponse) -> Void in
                 print(JSONResponse)
                 
                    if (JSONResponse["status"].int == 400 ) {
                        
                        let msgErr = JSONResponse["errors"].string
                        if ( msgErr == "User Not Found") {
                            Utils.snackBar(message: "Votre compte n'existe pas!!")
                            self.dismissKeyboard()

                        } else {
                            Utils.snackBar(message: "Mot de passe incorrecte")
                               self.dismissKeyboard()

                        }
                        
                    }else {
                        
                        let data = JSONResponse["data"]
                        let user : User = User()
                        user.token = data["token"].string!
                        let objUser = data["user"]
                        user.email = objUser["email"].string!
                        user.id = objUser["_id"].string!
                         user.first_name = objUser["first_name"].string!
                        user.last_name = objUser["last_name"].string!
                         user.profilePictureUrl =  objUser["profilePictureUrl"].string!
                        
                        Utils.saveUserData(user: user, type: .Normal)

                        self.stopAnimating()

                        
                        self.show(self.startMainViewController!, sender: nil)
                    }
               
                    self.stopAnimating()

                 
                 
                 
                 }, failure: {  (error) -> Void in
                 print(error)
                 
                    self.stopAnimating()
                 
                 // 400 (  "Wrong Password"  )
                 // This String Should Be an Email
                    Utils.snackBar(message: "Vérifier votre connection internet  !!")
                    self.dismissKeyboard()
                 
                 
                 })
 

            }
          /*********************END MY SERVICE WEB *******************/
            else {
                
            }

            
        }else{
            print("****************** No Internet  Connection")
            /*************** Check your Network **************/
 
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/

        }
  
    }

    
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        print("loginButtonDidCompleteLogin")
        if (NetWork.isConnectedToNetwork() == true){
            
            
        
        switch result {
        case .failed(let error):
            print(error)
        case .cancelled:
            print("User cancelled login.")
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            print("Logged in!")
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name,last_name, picture.type(large),email,updated_time"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    
                    if let userDict = result as? NSDictionary {
                        let first_Name = userDict["first_name"] as! String
                        let last_Name = userDict["last_name"] as! String
                        let id = userDict["id"] as! String
                        let email = userDict["email"] as! String
                        print(first_Name)
                        print(last_Name)
                        print(id)
                        print(email)
                        print(userDict)
                        // self.show(active : true,name:first_Name)
                        //startMainViewController
                     print("ccccccccc fuckfuckfuckfuckfuckfuckfuckfuckfuckfuck ")
                        self.show(self.startMainViewController!, sender: nil)

                    }
                }
            })
        }
        
        }else{ // Else No Connection
            
            
            print("****************** No Internet  Connection")
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
            
            
        }

    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        
      //  show(active : false,name : "Login by Facebook")
        print("User Logged Out")
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    
    /* Custom UI login */
    
    @IBAction func btnAction(_ sender: UIButton) {
        
        
        if (NetWork.isConnectedToNetwork() == true){
            
            
            let loginManager = LoginManager()
            if AccessToken.current == nil {
                
                loginManager.logIn([ .publicProfile,.email,.userFriends ], viewController: self) { loginResult in
                    switch loginResult {
                    case .failed(let error):
                        print(error)
                    case .cancelled:
                        print("User cancelled login.")
                    case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                        print("Logged in!")
                        
                        
                        let size = CGSize(width: 60, height: 60)
                        self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
                        
                        FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name,last_name, picture.type(large),email,updated_time"]).start(completionHandler: { (connection, result, error) -> Void in
                            if (error == nil){
                              
                                if let userDict = result as? NSDictionary {
                                    
                                    let first_Name = userDict["first_name"] as! String
                                    let last_Name = userDict["last_name"] as! String
                                    let id = userDict["id"] as! String
                                    let email = userDict["email"] as! String
                                    let name = userDict["name"] as! String
                                    
                                    print("TOOOOOOOOOOOOKEN" , accessToken.authenticationToken )
                                    print(first_Name)
                                    print(last_Name)
                                    print(id)
                                    print(email)
                                    
                                    
                                    print(userDict)
                                    let obj1urlImage = (userDict["picture"] as!  [String:AnyObject] )
                                    let obj1arrayImage = obj1urlImage ["data"]?["url"] as! String
                                    
                                    print("image " ,obj1arrayImage)
                                    let currentUserFB : User = User()
                                    currentUserFB.first_name = first_Name
                                    currentUserFB.last_name = last_Name
                                    currentUserFB.id = id
                                    currentUserFB.email = email
                                    currentUserFB.full_name = name
                                    currentUserFB.profilePictureUrl = obj1arrayImage
                              
                                   
                                    
                                    let url = "https://tuniservices-server.herokuapp.com/api/v1/auth/login/facebook"
                                    let param = ["email": currentUserFB.email,
                                                 "token": accessToken.authenticationToken ,
                                                 "first_name" : "\(currentUserFB.first_name)",
                                                 "last_name" : "\(currentUserFB.last_name)",
                                                 "profilePictureUrl" : currentUserFB.profilePictureUrl]
                                    AFWrapper.requestPOSTURL(url, params: param as [String : AnyObject], success: {   (JSONResponse) -> Void in
                                        print(JSONResponse)
                                        
                                        if (JSONResponse["status"].int == 400 ) {
                                            
                                            let msgErr = JSONResponse["errors"].string
                                            if ( msgErr == "User Not Found") {
                                              //  Utils.snackBar(message: "Votre compte n'existe pas!!")
                                                self.dismissKeyboard()
                                                
                                            } else {
                                               // Utils.snackBar(message: "Mot de passe incorrecte")
                                                self.dismissKeyboard()
                                                
                                            }
                                            
                                        }else {
                                            let data = JSONResponse["data"]
                                            let user : User = User()
                                            user.token = data["token"].string!
                                            let objUser = data["user"]
                                            user.email = objUser["email"].string!
                                            user.id = objUser["_id"].string!
                                            user.first_name = objUser["first_name"].string!
                                            user.last_name = objUser["last_name"].string!
                                            user.profilePictureUrl =  objUser["profilePictureUrl"].string!
                                            
                                            self.stopAnimating()

                                            Utils.saveUserData(user: user, type: .Facebook)

                                            
                                            self.show(self.startMainViewController!, sender: nil)
                                          }
                                      
                                        self.stopAnimating()

                                        
                                        
                                    }, failure: {  (error) -> Void in
                                        print(error)
                                        
                                        
                                        // 400 (  "Wrong Password"  ) 
                                        // This String Should Be an Email
                                        Utils.snackBar(message: "Vérifier votre connection internet  !!")
                                        self.dismissKeyboard()
                                        self.stopAnimating()

                                        
                                    })
                                    
                                    self.show(self.startMainViewController!, sender: nil)

                                    
                                    //self.show(active : true,name:first_Name)
                                 }
                            }else {
                                self.stopAnimating()
                            }
                        })
                    }
                }
            }else{
                
                loginManager.logOut()
              //  show(active : false,name:"Login by Facebook")
            }

            
        } else { // No Internet  Connection
            
            print("****************** No Internet  Connection")
            /*************** Check your Network **************/
            
            let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NetworkCnxViewContoller") as! NetworkCnxViewContoller
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            /************** End Check your network *************/
        
        }
        
        
        
        
    }
    
 
   
    
    @IBAction func didLoginAction(_ sender: UITextField) {
        self.txtLogin.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
    }
   
    @IBAction func didPasswordAction(_ sender: UITextField) {
        self.txtLogin.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
        self.txtPassword.layer.borderColor = UIColor(hexString: "#E3E4E5").cgColor
    }
    //Calls this function when the tap is recognized.
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
        
        self.navigationController?.navigationBar.gestureRecognizers?.removeAll()
        self.navigationController?.view.gestureRecognizers?.removeAll()
        
        
        
    }
}



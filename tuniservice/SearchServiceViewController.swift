//
//  SearchServiceViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 17/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class SearchServiceViewController: UIViewController , UITableViewDelegate, UITableViewDataSource ,UISearchBarDelegate , NVActivityIndicatorViewable{
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var viewNoData: UIView!
    
    @IBOutlet weak var loading: NVActivityIndicatorView!

    @IBOutlet weak var searchControllerView: UISearchBar!
    
    var tabB : [Service] = []
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.searchControllerView.delegate = self
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        loading.type = .ballClipRotate
        loading.color = UIColor.red
       
        
        if (tabB.count == 0 ) {
            viewNoData.isHidden = false
        }else {
            viewNoData.isHidden = true

        }
        
        //Exit
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(SearchServiceViewController.closepopup(_:)))
        viewNoData.isUserInteractionEnabled = true
        viewNoData.addGestureRecognizer(tapGestureRecognizer)
        
    }
    /*********** Close Pop up when click hors view login ******/
    func closepopup(_ sender:AnyObject){
        
        self.dismiss(animated: true, completion: nil)
        
    }
    /*********** END Close Pop up when click hors view login ******/
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //filteredData.count
        return tabB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSearch")
        
        cell?.textLabel?.text = tabB[indexPath.row].service
        
        return cell!
        
    }
    
    func search ( word : String ){
        self.tabB = []
        self.tableView.reloadData()
        loading.isHidden = false
        loading.startAnimating()
            AFWrapper.requestGETURL("https://tuniservices-server.herokuapp.com/api/v1/services/query/%7B%22fr.service%22:%7B%22$regex%22:%22\(word)%22%7D%7D?fields=%7B%22fr%22:1%7D", success: {
                (JSONResponse) -> Void in
                print("yes")
                let dataArray = JSONResponse["data"].array
                
                for data in dataArray! {
                    print(data)
                    let service : Service = Service()
                    service._id = data["_id"].string!
                    service.service = data["fr"]["service"].string!
                    service.descrip = data["fr"]["description"].string!
                    self.tabB.append(service)
                }
                print("HELLO",self.tabB.count)
                if ( self.tabB.count == 0 ){
                    self.viewNoData.isHidden = false
                }else {
                    self.viewNoData.isHidden = true
                }
             self.tableView.reloadData()
                self.loading.stopAnimating()
                self.loading.isHidden = true

            })
            {
                (error) -> Void in
                print("erreur")
                self.loading.stopAnimating()
                self.loading.isHidden = true

                print(error)
        }
        
    }
    
    // This method updates filteredData based on the text in the Search Box
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // When there is no text, filteredData is the same as the original data
        // When user has entered text into the search box
        // Use the filter method to iterate over all items in the data array
        // For each item, return true if the item should be included and false if the
        // item should NOT be included
       /* filteredData = searchText.isEmpty ? tabB : tabB.filter { (item: String) -> Bool in
            // If dataItem matches the searchText, return true to include it
            //let s = "\(item.idBordereaux)"
            // let dest = item.destinataire!
            // let Gov = item.destinataireGouvernorat!
            let s : String = ""
            return  (( s.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil ) )*/
        
            print(searchText)
            search(word: searchText.lowercased())
     //   }
        
      //  tableView.reloadData()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchControllerView.showsCancelButton = true
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchControllerView.showsCancelButton = false
        searchControllerView.text = ""
        searchControllerView.resignFirstResponder()
        self.tabB = []
        if ( self.tabB.count == 0 ){
            self.viewNoData.isHidden = false
        }else {
            self.viewNoData.isHidden = true
        }
        self.tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ListProviderViewController") as! ListProviderViewController
        popOverVC.service = tabB[indexPath.row]
        self.show(popOverVC, sender: nil)
        
        //self.dismiss(animated: true, completion: nil)
    }
}

//
//  ProfileViewController.swift
//  tuniservice
//
//  Created by MacBook Pro on 18/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import SideMenu

import SDWebImage
import NVActivityIndicatorView

class ProfileViewController: UIViewController , NVActivityIndicatorViewable  {

    @IBOutlet weak var viewPassword: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var email: UITextField!

    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var full_name: UILabel!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var itemBtnMenu: UIBarButtonItem!
    
    @IBOutlet weak var img: UIImageView!
    
    var editable : Bool = false
    
    @IBOutlet weak var btnImgEdit: UIButton!
    
    var user : User? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        btnSave.isHidden = true
        viewPassword.isHidden = true
        
   
        btnImgEdit.setImage(#imageLiteral(resourceName: "edit"), for: .normal)
        //ic_cancel
        
        let logo = UIImage(named: "smallIcon")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        // Do any additional setup after loading the view.
        img.layer.masksToBounds = true
        img.layer.cornerRadius = img.frame.width/2
        
        editable = false
        changeTxtToLabel()
        
        user = Utils.readUserData(from: "Profile")
        
        email.text = user?.email
        lastName.text = user?.last_name
        firstName.text = user?.first_name
        full_name.text = (user?.first_name)! + " " + (user?.last_name)!
        
        
        //Download Photo
        let url = URL(string: (user?.profilePictureUrl)!)
        //  photoProfile.kf.setImage(with: url)
        
        
        self.img.sd_setImage(with: url, placeholderImage: UIImage(named:"img1"), options: [SDWebImageOptions.continueInBackground, SDWebImageOptions.highPriority, SDWebImageOptions.refreshCached, SDWebImageOptions.handleCookies, SDWebImageOptions.retryFailed]) { (image, error, cacheType, url) in
            if error != nil {
                print("Failed: \(error)")
            } else {
                print("Success")
            }}
        

        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func EditAction(_ sender: Any) {
        
        if (editable == false ) {
            changeLabelToTxt()
            editable = true

            //edit
        }else {
            changeTxtToLabel()
            editable = false

            //edit
        }
    }
    
    func changeLabelToTxt(){
        password.isEnabled = true
        email.isEnabled = true

        lastName.isEnabled = true
        firstName.isEnabled = true
        
        viewPassword.isHidden = false
        btnSave.isHidden = false
        
        email.isSelected = true
  
        //edit
        
        btnImgEdit.setImage(#imageLiteral(resourceName: "ic_cancel"), for: .normal)


        
    }
    
    func changeTxtToLabel(){
        password.isEnabled = false
        email.isEnabled = false

        lastName.isEnabled = false
        firstName.isEnabled = false
        
        viewPassword.isHidden = true
        btnSave.isHidden = true
        btnImgEdit.setImage(#imageLiteral(resourceName: "edit"), for: .normal)

        //edit

    }

    @IBAction func btnMenuAction(_ sender: Any) {
            present(SideMenuManager.menuLeftNavigationController!, animated: true, completion: nil)
    }

    @IBAction func saveAction(_ sender: Any) {
        changeTxtToLabel()
        
        updateDataUser(token : (self.user?.token)!)
       // refreshDataUser()
        editable = false

    }
    
    func updateDataUser( token : String){
        
        let size = CGSize(width: 60, height: 60)
        self.startAnimating(size, message: "", type: NVActivityIndicatorType(rawValue: 16)!)
        //Check Token
        let url = "https://tuniservices-server.herokuapp.com/api/v1/secure/profile"
        let headerP = ["Authorization": "Bearer \(token)",
            "Content-Type" : "application/json"]
        let param = [ "last_name" : lastName.text?.trimmingCharacters(in: .whitespacesAndNewlines) ,
                      "first_name" : firstName.text?.trimmingCharacters(in: .whitespacesAndNewlines)]
        AFWrapper.requestPATCHURL(url, params: param as! [String : AnyObject], headers : headerP , success: {   (JSONResponse) -> Void in
            print(JSONResponse)
            
            if (JSONResponse["status"].int == 400 ) {
                
                let msgErr = JSONResponse["errors"].string
                if ( msgErr == "User Not Found") {
                    //  Utils.snackBar(message: "Votre compte n'existe pas!!")
                    
                    self.stopAnimating()
                    self.email.text = self.user?.email
                    self.lastName.text = self.user?.last_name
                    self.firstName.text = self.user?.first_name
                    self.full_name.text = (self.user?.first_name)! + " " + (self.user?.last_name)!
                } else {
                    // Utils.snackBar(message: "Mot de passe incorrecte")
                    self.stopAnimating()
                    self.email.text = self.user?.email
                    self.lastName.text = self.user?.last_name
                    self.firstName.text = self.user?.first_name
                    self.full_name.text = (self.user?.first_name)! + " " + (self.user?.last_name)!

                }
                
            }else {
                
                let data = JSONResponse["data"]
                
                let objUser = data["user"]
                self.user?.first_name = objUser["first_name"].string!
                self.user?.last_name = objUser["last_name"].string!
                
                self.email.text = self.user?.email
                self.lastName.text = self.user?.last_name
                self.firstName.text = self.user?.first_name
                self.full_name.text = (self.user?.first_name)! + " " + (self.user?.last_name)!
                
                Utils.saveUserData(user: self.user!, type: (self.user?.accountType)!)
             
                
                Utils.snackBar(message: "Votre mise à jour bien effectuer")

                
                /************************************/
                if (self.password.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""){
                    
                    let url = "https://tuniservices-server.herokuapp.com/api/v1/secure/profile/changePassword"
                    let headerP = ["Authorization": "Bearer \(self.user?.token))",
                        "Content-Type" : "application/json"]
                    let parame = ["password": self.password.text?.trimmingCharacters(in: .whitespacesAndNewlines)]
                    AFWrapper.requestPOSTURL(url, params: parame as [String : AnyObject], headers: headerP, success: { (JSONResponse) -> Void in
                        
                        print("passswordd changed")
                        print(JSONResponse)
                        
                        
                            self.stopAnimating()
                        Utils.snackBar(message: "Votre mot de passe est changer")

                        
                    } , failure: {  (error) -> Void in
                        print(error)
                        Utils.snackBar(message: "Vérifier votre connection internet")
                        self.stopAnimating()

                    })

                }else {
                    self.stopAnimating()

                }
                
                /************************************/
                
                
                
            }
            
            
        }, failure: {  (error) -> Void in
            print(error)
            self.email.text = self.user?.email
            self.lastName.text = self.user?.last_name
            self.firstName.text = self.user?.first_name
            self.full_name.text = (self.user?.first_name)! + " " + (self.user?.last_name)!
            print("errrrrrrrrrrrrrrrrr updateDataUser")
            // 400 (  "Wrong Password"  )
            // This String Should Be an Email
            Utils.snackBar(message: "Vérifier votre connection internet  !!")
            
            
        })

        
    }
    
    
    func refreshDataUser(){
        let user : User = Utils.getUserFromServer(token: self.user!.token, type: (self.user?.accountType)!)
        
    }
}

//
//  Utils.swift
//  tuniservice
//
//  Created by MacBook Pro on 10/08/2017.
//  Copyright © 2017 tuniservice. All rights reserved.
//

import UIKit
import TTGSnackbar
enum typeAccount {
    case Google  
    case Facebook
    case Normal
}
class Utils: NSObject {
    
    
    //
    /*
     import KeychainSwift
                    -- Keychain --
     
     Stores sensitive data such as passwords  , certificates , tokens ect ..
     Is implemented as SQLite Database
     Application can Accesss only items in its keychain-access-group
     Can be arbitarily read on a jailbroken device using keychain-dumper
     
     */
    
    /*
                  -- Application SandBox --
     
     */
    

    static let preferences =  UserDefaults.standard

    
    public static func snackBar ( message : String ){
        let snackbar: TTGSnackbar = TTGSnackbar.init(message: message, duration:  .middle)
        
        // Change the content padding inset
        snackbar.contentInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
        
        // Change margin
        snackbar.leftMargin = 8
        snackbar.rightMargin = 8
        
        // Change message text font and color
        snackbar.messageTextColor = UIColor.white
        snackbar.messageTextFont = UIFont.boldSystemFont(ofSize: 18)
        
        // Change snackbar background color
        snackbar.backgroundColor = UIColor(hexString: "#3d2b1f")
        snackbar.backgroundColor?.withAlphaComponent(0.4)
        // Change animation duration
        snackbar.animationDuration = 0.5
        
        // Animation type
        snackbar.animationType = .slideFromBottomBackToBottom
        
        snackbar.show()
        
    }
    
    public static func  isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    public static func userDataExist() -> Bool{
       
        
        let email : String? = preferences.string(forKey: "currentUserEmail")
        print("userDataExist ",email)
        
        return !( ( email == nil ) || (email == "" ) )
    }
    
    
    public static func saveUserData( user : User , type : typeAccount) -> Bool{
        preferences.set(user.email, forKey: "currentUserEmail")
        preferences.set(user.first_name, forKey: "currentUserFirstName")
        preferences.set(user.last_name, forKey: "currentUserLastName")
        preferences.set(user.id, forKey: "currentUserId")
        preferences.set(user.password, forKey: "currentUserPassword")
        preferences.set(user.profilePictureUrl, forKey: "currentUserProfilePictureUrl")
        preferences.set(user.token, forKey: "currentUserToken")
        
        if ( type == .Normal  ){
            preferences.set("Normal", forKey: "typeAccount")

        }else if ( type == .Facebook ) {
            preferences.set("Facebook" , forKey : "typeAccount")
        } else {
            preferences.set("Google" , forKey : "typeAccount")
        }
        
        let didSave = preferences.synchronize()
        return didSave
     }
    
    public static func readUserData( from : String)  -> User{
        print("from from ",from)
        let preferences =  UserDefaults.standard
        
        let user : User = User()
   
        user.email = preferences.string(forKey: "currentUserEmail")!
        user.first_name = preferences.string(forKey: "currentUserFirstName")!
      
        user.last_name = preferences.string(forKey: "currentUserLastName")!
        user.id = preferences.string(forKey: "currentUserId")!
        
        user.password = preferences.string(forKey: "currentUserPassword")!
        user.profilePictureUrl = preferences.string(forKey: "currentUserProfilePictureUrl")!
        
        user.token = preferences.string(forKey: "currentUserToken")!

        let type = preferences.string(forKey: "typeAccount")!
        if (type == "Facebook" ){
            user.accountType = .Facebook
        }else if (type == "Google"){
            user.accountType = .Google
        }else {
            user.accountType = .Normal
        }
        
        return user
    }
    
    public static func removeUser() -> Bool{
        
        let preferences =  UserDefaults.standard

        preferences.removeObject(forKey: "currentUserEmail")
        preferences.removeObject(forKey: "currentUserFirstName")
        
        
        preferences.removeObject(forKey: "currentUserLastName")
        preferences.removeObject(forKey: "currentUserId")
        
        
        preferences.removeObject(forKey: "currentUserPassword")
        preferences.removeObject(forKey: "currentUserProfilePictureUrl")
        
        preferences.removeObject(forKey: "currentUserToken")

        preferences.removeObject(forKey: "typeAccount")

        preferences.synchronize()
        return preferences.synchronize()
    }
    
    public static func changePassword ( token : String , password : String ) {
        let url = "https://tuniservices-server.herokuapp.com/api/v1/secure/profile/changePassword"
        let headerP = ["Authorization": "Bearer \(token)",
            "Content-Type" : "application/json"]
        let parame = ["password": password]
        AFWrapper.requestPOSTURL(url, params: parame as [String : AnyObject], headers: headerP, success: { (JSONResponse) -> Void in
            
            print("passswordd changed")
            print(JSONResponse)
            
            if (JSONResponse["status"].int == 400 ) {
                
                let msgErr = JSONResponse["errors"].string
                if ( msgErr == "User Not Found") {
                    //  Utils.snackBar(message: "Votre compte n'existe pas!!")
                    Utils.snackBar(message: "erreur")

                    
                } else {
                    // Utils.snackBar(message: "Mot de passe incorrecte")
                    Utils.snackBar(message: "erreur")

                }
                
            }else {
                
            }
            
        } , failure: {  (error) -> Void in
            print(error)
              Utils.snackBar(message: "Vérifier votre connection internet")
            
        })
        
        
    }

    
    public static func getUserFromServer ( token : String , type : typeAccount) -> User{
        let user : User = User()
        let url = "https://tuniservices-server.herokuapp.com/api/v1/auth/info"
        let headerP = ["Authorization": "Bearer \(token)",
            "Content-Type" : "application/json"]
        AFWrapper.requestGETwithParamsURL(url, params: [:] , headers : headerP , success: {   (JSONResponse) -> Void in
            print(JSONResponse)
            
            
            
            if (JSONResponse["status"].int == 400 ) {
                
                let msgErr = JSONResponse["errors"].string
                if ( msgErr == "User Not Found") {
                    //  Utils.snackBar(message: "Votre compte n'existe pas!!")
                    
                    
                } else {
                    // Utils.snackBar(message: "Mot de passe incorrecte")
                    
                }
                
            }else {
                
                let data = JSONResponse["data"]
                
                let objUser = data["user"]
                user.email = objUser["email"].string!
                user.id = objUser["_id"].string!
                user.first_name = objUser["first_name"].string!
                user.last_name = objUser["last_name"].string!
                user.profilePictureUrl =  objUser["profilePictureUrl"].string!
                user.accountType = type
              
                
                
            }
            
            
        }, failure: {  (error) -> Void in
            print(error)
            
            print("errrrrrrrrrrrrrrrrr getUserFromServer")
            // 400 (  "Wrong Password"  )
            // This String Should Be an Email
            Utils.snackBar(message: "Vérifier votre connection internet  !!")
            
            
        })
        

        
        return user

    }
    
}

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.characters.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension CAShapeLayer {
    func drawRoundedRect(rect: CGRect, andColor color: UIColor, filled: Bool) {
        fillColor = filled ? color.cgColor : UIColor.white.cgColor
        strokeColor = color.cgColor
        path = UIBezierPath(roundedRect: rect, cornerRadius: 7).cgPath
    }
}

private var handle: UInt8 = 0;

extension UIBarButtonItem {
    private var badgeLayer: CAShapeLayer? {
        if let b: AnyObject = objc_getAssociatedObject(self, &handle) as AnyObject? {
            return b as? CAShapeLayer
        } else {
            return nil
        }
    }
    
    func setBadge(text: String?, withOffsetFromTopRight offset: CGPoint = CGPoint.zero, andColor color:UIColor = UIColor.red, andFilled filled: Bool = true, andFontSize fontSize: CGFloat = 11)
    {
        badgeLayer?.removeFromSuperlayer()
        
        if (text == nil || text == "") {
            return
        }
        
        addBadge(text: text!, withOffset: offset, andColor: color, andFilled: filled)
    }
    
    private func addBadge(text: String, withOffset offset: CGPoint = CGPoint.zero, andColor color: UIColor = UIColor.red, andFilled filled: Bool = true, andFontSize fontSize: CGFloat = 11)
    {
        guard let view = self.value(forKey: "view") as? UIView else { return }
        
        var font = UIFont.systemFont(ofSize: fontSize)
        
        if #available(iOS 9.0, *) {
            font = UIFont.monospacedDigitSystemFont(ofSize: fontSize, weight: UIFontWeightRegular)
        }
        
        let badgeSize = text.size(attributes: [NSFontAttributeName: font])
        
        // Initialize Badge
        let badge = CAShapeLayer()
        
        let height = badgeSize.height;
        var width = badgeSize.width + 2 /* padding */
        
        //make sure we have at least a circle
        if (width < height) {
            width = height
        }
        
        //x position is offset from right-hand side
        let x = view.frame.width - width + offset.x
        
        let badgeFrame = CGRect(origin: CGPoint(x: x, y: offset.y), size: CGSize(width: width, height: height))
        
        badge.drawRoundedRect(rect: badgeFrame, andColor: color, filled: filled)
        view.layer.addSublayer(badge)
        
        // Initialiaze Badge's label
        let label = CATextLayer()
        label.string = text
        label.alignmentMode = kCAAlignmentCenter
        label.font = font
        label.fontSize = font.pointSize
        
        label.frame = badgeFrame
        label.foregroundColor = filled ? UIColor.white.cgColor : color.cgColor
        label.backgroundColor = UIColor.clear.cgColor
        label.contentsScale = UIScreen.main.scale
        badge.addSublayer(label)
        
        // Save Badge as UIBarButtonItem property
        objc_setAssociatedObject(self, &handle, badge, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
    private func removeBadge() {
        badgeLayer?.removeFromSuperlayer()
    }
}
extension String {
    
    subscript (i: Int) -> Character {
        return self[index(startIndex, offsetBy: i)]
    }
    
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    
    subscript (r: Range<Int>) -> String {
        let start = index(startIndex, offsetBy: r.lowerBound)
        let end = index(startIndex, offsetBy: r.upperBound - r.lowerBound)
        return self[Range(start ..< end)]
    }
}
